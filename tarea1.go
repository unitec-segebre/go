package main

import (
  "fmt"
  "net/http"
  "encoding/json"
  "log"
  "strings"
  "strconv"
  "regexp"
  "encoding/base64"
  //"encoding/binary"
  //"reflect"
  )

type request1Body struct{
  Origen  string  `json:"origen"`
  Destino string  `json:"destino"`
}

type request1Response struct{
  Lat float64 `json:"lat"`
  Lng float64 `json:"lon"`
}

type request2Body struct{
  Origen  string  `json:"origen"`
}

type request2Response struct{
  Name  string  `json:"nombre"`
  Lat   float64 `json:"lat"`
  Lng   float64 `json:"lon"`
}

type request3BodyAnd34Response struct{
  Name  string  `json:"nombre"`
  Data string  `json:"data"`
}

func main() {

  fmt.Println("Server up and running!")
  http.HandleFunc("/ejercicio1", handler_ejercicio1)
  http.HandleFunc("/ejercicio2", handler_ejercicio2)
  http.HandleFunc("/ejercicio3", handler_ejercicio3)
  log.Fatal(http.ListenAndServe(":8080", nil))

}

func handler_ejercicio1(w http.ResponseWriter, r *http.Request) {
  if r.Method == "POST"{
    var request1BodyValues request1Body
    if r.Body == nil{
      Err400(&w)
      return
    }
    defer r.Body.Close()
    err := json.NewDecoder(r.Body).Decode(&request1BodyValues)
    if err != nil{
      Err400(&w)
      return
    }
    r, _ := regexp.Compile("(\\w,\\w*)+")
    if !(r.MatchString(request1BodyValues.Origen)&&r.MatchString(request1BodyValues.Destino)){
      Err400(&w)
      return
    }
    mapsLink := "https://maps.googleapis.com/maps/api/directions/json?origin=ORIGIN&destination=DESTINATION&key=AIzaSyA7sQSQEOesLMKtCLmqISRpv7YHeWL67-c"
    request1BodyValues.Origen = strings.Replace(request1BodyValues.Origen, " ", "+", -1)
    request1BodyValues.Destino = strings.Replace(request1BodyValues.Destino, " ", "+", -1)
    mapsLink = strings.Replace(mapsLink, "ORIGIN", request1BodyValues.Origen, 1)
    mapsLink = strings.Replace(mapsLink, "DESTINATION", request1BodyValues.Destino, 1)
    fmt.Println("Link Request:", mapsLink)

    mapsResp, err := http.Get(mapsLink)
    if err != nil{
      Err500(&w)
      return
    }

    defer func() {
      if recover()!=nil{
        Err500(&w)
      }
    }()

    var routOnMaps interface{}
    err = json.NewDecoder(mapsResp.Body).Decode(&routOnMaps)
    routes := routOnMaps.(map[string]interface{})["routes"].([]interface{})[0].(map[string]interface{})["legs"].([]interface{})[0].(map[string]interface{})["steps"].([]interface{})
    var ruta []request1Response
    var temp request1Response
    
    temp.Lat = routes[0].(map[string]interface{})["start_location"].(map[string]interface{})["lat"].(float64)
    temp.Lng = routes[0].(map[string]interface{})["start_location"].(map[string]interface{})["lng"].(float64)
    ruta = append(ruta, temp)
    for _, location := range routes{
      temp.Lat = location.(map[string]interface{})["end_location"].(map[string]interface{})["lat"].(float64)
      temp.Lng = location.(map[string]interface{})["end_location"].(map[string]interface{})["lng"].(float64)
      ruta = append(ruta, temp)
    }

    response := make(map[string]interface{})
    response["ruta"] = ruta
    w.Header().Set("Content-Type", "application/json")
    json.NewEncoder(w).Encode(response)
  }
}

func handler_ejercicio2(w http.ResponseWriter, r *http.Request) {
  if r.Method == "POST"{
    var request2BodyValues request2Body
    if r.Body == nil{
      Err400(&w)
      return
    }
    defer r.Body.Close()
    err := json.NewDecoder(r.Body).Decode(&request2BodyValues)
    if err != nil{
      Err400(&w)
      return
    }
    r, _ := regexp.Compile("(\\w,\\w*)+")
    if !(r.MatchString(request2BodyValues.Origen)){
      Err400(&w)
      return
    }
    coordinatesLink := "https://maps.googleapis.com/maps/api/geocode/json?address=ADDRESS&key=AIzaSyDxkk38M1uRTyD6vw7OyBUQ8x_2W2qOsEU"
    request2BodyValues.Origen = strings.Replace(request2BodyValues.Origen, " ", "+", -1)
    coordinatesLink = strings.Replace(coordinatesLink, "ADDRESS", request2BodyValues.Origen, 1)
    fmt.Println("Link Request:", coordinatesLink)

    mapsResp, err := http.Get(coordinatesLink)
    if err != nil{
      Err500(&w)
      return
    }

    defer func() {
      if recover()!=nil{
        Err500(&w)
      }
    }()

    var googleAPIsResponse interface{}
    err = json.NewDecoder(mapsResp.Body).Decode(&googleAPIsResponse)
    coordinates := googleAPIsResponse.(map[string]interface{})["results"].([]interface{})[0].(map[string]interface{})["geometry"].(map[string]interface{})["location"].(map[string]interface{})

    nearMeLink := "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=LAT,LNG&radius=3000&types=food&name=cruise&key=AIzaSyCx14BVgeJ89yixorOA7gaab-uscUWlNFU"
    nearMeLink = strings.Replace(nearMeLink, "LAT", strconv.FormatFloat(coordinates["lat"].(float64), 'f', -1, 64), 1)
    nearMeLink = strings.Replace(nearMeLink, "LNG", strconv.FormatFloat(coordinates["lng"].(float64), 'f', -1, 64), 1)
    fmt.Println(nearMeLink)
    mapsResp, err = http.Get(nearMeLink)
    if err != nil{
      Err500(&w)
      return
    }

    err = json.NewDecoder(mapsResp.Body).Decode(&googleAPIsResponse)
    nearMeRestaurants := googleAPIsResponse.(map[string]interface{})["results"].([]interface{})
    var restaurants []request2Response
    var temp request2Response
    for _, location := range nearMeRestaurants{
      temp.Name = location.(map[string]interface{})["name"].(string)
      temp.Lat = location.(map[string]interface{})["geometry"].(map[string]interface{})["location"].(map[string]interface{})["lat"].(float64)
      temp.Lng = location.(map[string]interface{})["geometry"].(map[string]interface{})["location"].(map[string]interface{})["lng"].(float64)
      restaurants = append(restaurants, temp)
    }

    response := make(map[string]interface{})
    response["restaurantes"] = restaurants
    w.Header().Set("Content-Type", "application/json")
    json.NewEncoder(w).Encode(response)

  }
}

func handler_ejercicio3(w http.ResponseWriter, r *http.Request) {
  if r.Method == "POST"{
    var request3BodyValues request3BodyAnd34Response
    if r.Body == nil{
      Err400(&w)
      return
    }
    defer r.Body.Close()
    err := json.NewDecoder(r.Body).Decode(&request3BodyValues)
    if err != nil{
      Err400(&w)
      return
    }

    defer func() {
      if recover()!=nil{
        Err500(&w)
      }
    }()


    decodedImg, _ := base64.StdEncoding.DecodeString(request3BodyValues.Data)
    decodedImgWidth := int(decodedImg[0x12])
    decodedImgHeight := int(decodedImg[0x16])
    decodedImgPixelArray := int(decodedImg[0x0A])
    for i:=0; i<decodedImgHeight; i++{
      for j:=0; j<decodedImgWidth; j++{
        pos := decodedImgPixelArray+(i*decodedImgWidth*4)+(j*4)//There are 4 bytes in each pixel
        greyPixel := (uint8(decodedImg[pos]) + uint8(decodedImg[pos+1]) + uint8(decodedImg[pos+2]))/3
        decodedImg[pos] = byte(greyPixel)
        decodedImg[pos+1] = byte(greyPixel)
        decodedImg[pos+2] = byte(greyPixel)
      }
    }
    request3BodyValues.Data = base64.StdEncoding.EncodeToString([]byte(decodedImg))
    request3BodyValues.Name = strings.Replace(request3BodyValues.Name, ".", "(blanco y negro).", 1)
    w.Header().Set("Content-Type", "application/json")
    json.NewEncoder(w).Encode(request3BodyValues)
  }
}

func Err400(w *http.ResponseWriter) {
  ErrMsg := map[string]string{"error": "No se especifico origen"}
  (*w).Header().Set("Content-Type", "application/json")
  (*w).WriteHeader(http.StatusBadRequest)
  json.NewEncoder(*w).Encode(ErrMsg)
}

func Err500(w *http.ResponseWriter) {
  ErrMsg := map[string]string{"error": "Opps, ha ocurrido un error :/"}
  (*w).Header().Set("Content-Type", "application/json")
  (*w).WriteHeader(http.StatusInternalServerError)
  json.NewEncoder(*w).Encode(ErrMsg)
}